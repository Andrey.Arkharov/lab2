package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private final int fridgeCapacity;
    ArrayList<FridgeItem> itemsInFridge;

    public Fridge() {           // initializing inside constructor (practice)
        fridgeCapacity = 20;
        itemsInFridge = new ArrayList<>(20);
    }

    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return fridgeCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (item == null) return false;

        int availableSize = fridgeCapacity - nItemsInFridge();
        if (availableSize > 0) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsInFridge.isEmpty()) throw new NoSuchElementException();

        FridgeItem removedItem = null;

        for (FridgeItem single : itemsInFridge) {
            if (item.equals(single)) {
                removedItem = single;
            }
        }

        itemsInFridge.remove(removedItem);
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();

        for (FridgeItem item : itemsInFridge) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        itemsInFridge.removeAll(expiredItems);

        return expiredItems;
    }
}
